//
//  ProgressView.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class ProgressView
{
    class func showProgress(texto: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: texto, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        return alert
    }
    
    class func showMessageWithImageView(image: String) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 250, height: 228))
        imageView.image = UIImage(named: image)
        
        alert.view.addSubview(imageView)
        return alert
    }
}
