//
//  ErrorHelper.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class ErrorHelper {
    
    class func createErro(mensagem: String, detalhes: String, status: Int) -> NSError {
        let userInfo: [NSObject: AnyObject] = [
            NSLocalizedDescriptionKey as NSObject : mensagem as AnyObject,
            NSLocalizedFailureReasonErrorKey as NSObject : detalhes as AnyObject
        ]
        return NSError(domain: "com.msn.julio_net", code: status, userInfo: userInfo as? [String : Any])
    }
}
