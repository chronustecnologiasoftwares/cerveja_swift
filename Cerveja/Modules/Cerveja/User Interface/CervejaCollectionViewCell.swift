//
//  CervejaTableViewCell.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class CervejaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var abvLabel: UILabel!
    
    func updateCell(cerveja: Cerveja) {
        self.nameLabel.text = cerveja.name
        self.abvLabel.text = "ABV: \(cerveja.abv)"
        self.imageImageView.image = getImage(cerveja: cerveja)
    }
    
    func getImage(cerveja: Cerveja) -> UIImage? {
        let imagem = CervejaDataManager.getById(id: Int32(cerveja.id))
        if imagem == nil {
            if let image = cerveja.image_url {
                self.downloadImage(id: cerveja.id, url: image)
                return nil
            } else {
                return UIImage(named: "imgDefault")
            }
        } else {
            if let image = imagem!.image {
                return UIImage(data: image)
            } else {
                return UIImage(named: "imgDefault")
            }
        }
    }
    
    func downloadImage(id: Int, url: String) {
        let url = NSURL(string: url)!
        let task = URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
            if (error == nil) {
                DispatchQueue.main.sync(execute: {
                    let retorno = CervejaDataManager.insert(id: Int32(id), image: data!)
                    if retorno {
                        self.imageImageView.image = UIImage(data: data!)
                    } else {
                        self.imageImageView.image = UIImage(named: "imgDefault")
                    }
                })
            } else {
                self.imageImageView.image = UIImage(named: "imgDefault")
            }
        }
        task.resume()
    }
    
}
