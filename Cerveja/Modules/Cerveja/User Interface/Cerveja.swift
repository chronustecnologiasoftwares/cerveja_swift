//
//  Cerveja.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class Cerveja: Codable {
    var id: Int = 0
    var name: String?
    var image_url: String?
    var abv: Double = 0.0
    var ibu: Double?
    var tagline: String?
    var description: String?
    
    init(id: Int, name: String, image_url: String, abv: Double, ibu: Double?, tagline: String, description: String) {
        self.id = id
        self.name = name
        self.image_url = image_url
        self.abv = abv
        self.ibu = ibu
        self.tagline = tagline
        self.description = description
    }
    
}
