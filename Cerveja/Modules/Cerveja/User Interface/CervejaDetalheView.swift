//
//  CervejaDetalheView.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class CervejaDetalheView: UIViewController {
    
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var abvLabel: UILabel!
    @IBOutlet weak var ibuLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var cerveja: Cerveja?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Cervejas"
        
        self.nameLabel.text = cerveja?.name
        self.tagLineLabel.text = cerveja?.tagline
        self.abvLabel.text = "Teor Alcóolico: \(cerveja?.abv ?? 0.0)"
        if let ibu = cerveja?.ibu {
            self.ibuLabel.text = "IBU: \(ibu)"
        } else {
            self.ibuLabel.text = "IBU:"
        }
        self.descriptionTextView.text = cerveja?.description
        
        if let imagem = CervejaDataManager.getById(id: Int32(cerveja!.id)) {
            self.imageImageView.image = UIImage(data: imagem.image!)
        } else {
            self.imageImageView.image = UIImage(named: "imgDefault")
        }
        
        if let url = cerveja?.image_url {
            self.downloadImage(url: url)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func downloadImage(url: String) {
        let url = NSURL(string: url)!
        let task = URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
            if (error == nil) {
                DispatchQueue.main.sync(execute: {
                    self.imageImageView.image = UIImage(data: data!)
                })
            } else {
                DispatchQueue.main.sync(execute: {
                    self.imageImageView.image = UIImage(named: "imgDefault")
                })
            }
        }
        task.resume()
    }
}
