//
//  CervejaPresenter.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class CervejaPresenter: CervejaViewToPresenterProtocol {
    
    var view: CervejaPresenterToViewProtocol?
    var interector: CervejaPresenterToInteractorProtocol?
    var router: CervejaPresenterToRouterProtocol?
    
    func selecionarTodos(pagina: Int) {
        self.interector?.selecionarTodos(pagina: pagina)
    }
    
    func loadCervejaDetalhe(cerveja: Cerveja, view: UIViewController) {
        self.router?.loadCervejaDetalhe(cerveja: cerveja, view: view)
    }
}

extension CervejaPresenter: CervejaInteractorToPresenterProtocol {
    
    func selecionarTodosSucesso(cervejas: [Cerveja]) {
        self.view?.selecionarTodosSucesso(cervejas: cervejas)
    }
    
    func selecionarTodosFalha(mensagem: String) {
        self.view?.selecionarTodosFalha(mensagem: mensagem)
    }
}
