//
//  CervejaView.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class CervejaView: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: CervejaViewToPresenterProtocol?
    var cervejas = [Cerveja]()
    var pagina = 1
    var stopdownload = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Cervejas"
        self.navigationItem.backBarButtonItem?.title = ""
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets.init(top: 10, left: 20, bottom: 10, right: 20)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: ((self.collectionView?.frame.size.width)!-100)/2, height: ((self.collectionView?.frame.size.width)!)/2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        presenter?.selecionarTodos(pagina: pagina)
    }
}

extension CervejaView: CervejaPresenterToViewProtocol {
    
    func selecionarTodosSucesso(cervejas: [Cerveja]) {
        if cervejas.count == 0 {
            self.stopdownload = true
        }
        
        for cerveja in cervejas {
            self.cervejas.append(cerveja)
        }
        self.collectionView.reloadData()
    }
    
    func selecionarTodosFalha(mensagem: String) {
        self.dismiss(animated: false) {
            let alert = UIAlertController(title: "Atençao", message: mensagem, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CervejaView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cervejas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCerveja", for: indexPath) as! CervejaCollectionViewCell
        cell.updateCell(cerveja: cervejas[indexPath.row])
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.8
        
        return cell
    }
}

extension CervejaView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.loadCervejaDetalhe(cerveja: self.cervejas[indexPath.row], view: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !self.stopdownload {
            pagina += 1
            presenter?.selecionarTodos(pagina: pagina)
        }
    }
}
