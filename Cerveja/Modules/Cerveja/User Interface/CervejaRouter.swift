//
//  CervejaRouter.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

class CervejaRouter: CervejaPresenterToRouterProtocol {
    
    static func loadModule() -> UIViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "CervejaView") as? CervejaView
        let presenter: CervejaViewToPresenterProtocol & CervejaInteractorToPresenterProtocol = CervejaPresenter();
        let interactor: CervejaPresenterToInteractorProtocol = CervejaInteractor();
        let router: CervejaPresenterToRouterProtocol = CervejaRouter();
        
        view?.presenter = presenter
        presenter.view = view
        presenter.interector = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view!
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func loadCervejaDetalhe(cerveja: Cerveja, view: UIViewController) {
        let itemView = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CervejaDetalheView") as? CervejaDetalheView
        itemView!.cerveja = cerveja
        view.navigationController?.pushViewController(itemView!, animated: true)
    }
}

