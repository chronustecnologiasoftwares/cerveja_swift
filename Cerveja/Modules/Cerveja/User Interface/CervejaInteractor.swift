//
//  CervejaInteractor.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class CervejaInteractor: CervejaPresenterToInteractorProtocol {
    
    var presenter: CervejaInteractorToPresenterProtocol?
    
    func selecionarTodos(pagina: Int) {
        CervejaController.selecionarTodos(pagina: pagina) { (cervejas: [Cerveja], error: NSError?) in
            if error != nil {
                self.presenter?.selecionarTodosFalha(mensagem: error!.localizedDescription)
            } else {
                self.presenter?.selecionarTodosSucesso(cervejas: cervejas)
            }
        }
    }
}
