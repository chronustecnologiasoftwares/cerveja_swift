//
//  CervejaController.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

class CervejaController {
    
    typealias Callback = (_ Cervejas: [Cerveja], _ error: NSError?) -> Void
    
    class func selecionarTodos(pagina: Int, callback: @escaping Callback) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = NSURL(string: "https://api.punkapi.com/v2/beers?page=\(pagina)")
        let task = session.dataTask(with: url! as URL) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.sync(execute: {
                    callback([], error as NSError?)
                })
            } else {
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        let decoder = JSONDecoder()
                        let retorno = try! decoder.decode([Cerveja].self, from: data!)
                        DispatchQueue.main.sync(execute: {
                            callback(retorno, nil)
                        })
                    } else {
                        let erro = ErrorHelper.createErro(mensagem: "Falha ao tentar acessar dados", detalhes: "Falha ao tentar acesso dados ", status: httpResponse.statusCode)
                        DispatchQueue.main.sync(execute: {
                            callback([], erro)
                        })
                    }
                }
            }
        }
        task.resume()
    }
}
