//
//  CervejaDataManager.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import CoreData

class CervejaDataManager {
    
    class func insert(id: Int32, image: Data) -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Imagem", in: context)
        let imagem = NSManagedObject(entity: entity!, insertInto: context)
        imagem.setValue(id, forKey: "id")
        imagem.setValue(image, forKey: "image")
        return appDelegate.saveContext()
    }
    
    class func getAll() -> [Imagem] {
        var images = [Imagem]()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        do {
            images = try context.fetch(Imagem.fetchRequest())
        } catch {
            
        }
        return images
    }
    
    class func getById(id: Int32) -> Imagem? {
        let images = getAll()
        
        let filtro = images.filter { (image) -> Bool in
            if image.id == id {
                return true
            } else {
                return false
            }
        }
        
        return filtro.count != 0 ? filtro[0] : nil
    }
}
