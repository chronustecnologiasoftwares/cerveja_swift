//
//  CervejaProtocols.swift
//  Cerveja
//
//  Created by José Júlio on 20/08/19.
//  Copyright © 2019 Jose Julio Junior. All rights reserved.
//

import Foundation
import UIKit

protocol CervejaPresenterToViewProtocol: class {
    func selecionarTodosSucesso(cervejas: [Cerveja])
    func selecionarTodosFalha(mensagem: String)
}

protocol CervejaInteractorToPresenterProtocol: class {
    func selecionarTodosSucesso(cervejas: [Cerveja])
    func selecionarTodosFalha(mensagem: String)
}

protocol CervejaPresenterToInteractorProtocol: class {
    var presenter: CervejaInteractorToPresenterProtocol? {get set}
    func selecionarTodos(pagina: Int)
}

protocol CervejaViewToPresenterProtocol: class {
    var view: CervejaPresenterToViewProtocol? {get set}
    var interector: CervejaPresenterToInteractorProtocol? {get set}
    var router: CervejaPresenterToRouterProtocol? {get set}
    func selecionarTodos(pagina: Int)
    func loadCervejaDetalhe(cerveja: Cerveja, view: UIViewController)
}

protocol CervejaPresenterToRouterProtocol: class {
    static func loadModule() -> UIViewController
    func loadCervejaDetalhe(cerveja: Cerveja, view: UIViewController)
}

